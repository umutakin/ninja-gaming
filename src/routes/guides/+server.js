import { json } from "@sveltejs/kit";

export function GET(event) {
    // log all headers
    //   console.log(...event.request.headers);

    const guides = [
        { id: 1, title: "some title 1" },
        { id: 2, title: "some title 2" },
        { id: 3, title: "some title 3" },
        { id: 4, title: "some title 4" },
        { id: 5, title: "some title 5" },
    ];

    // return json(guides);

    return json({
        guides,

        // retrieve a specific header
        // userAgent: event.request.headers.get('user-agent')
    });
}
