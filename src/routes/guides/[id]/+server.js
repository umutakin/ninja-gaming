import { error } from "@sveltejs/kit";
import { json } from "@sveltejs/kit";

export function GET({ params }) {
    // log all headers
    //   console.log(...event.request.headers);

    const guides = [
        { id: 1, title: "some title 1", body: "lorem ipsum 1" },
        { id: 2, title: "some title 2", body: "lorem ipsum 2" },
        { id: 3, title: "some title 3", body: "lorem ipsum 3" },
        { id: 4, title: "some title 4", body: "lorem ipsum 4" },
        { id: 5, title: "some title 5", body: "lorem ipsum 5" },
    ];

    console.log(params);

    const guide = guides.find((g) => g.id == params.id);

    if (!guide) {
        throw error(404, { message: "ERROR" });
    }

    return json({
        guide,
    });

    // return json(guides);

    return json({
        guides,

        // retrieve a specific header
        // userAgent: event.request.headers.get('user-agent')
    });
}
