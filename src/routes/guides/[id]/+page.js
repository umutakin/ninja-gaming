import { redirect } from "@sveltejs/kit";

export async function load({ fetch, params }) {
    const id = params.id;
    // const res = await fetch(`https://jsonplaceholder.typicode.com/posts/${id}`);
    const res = await fetch(`/guides/${id}`);
    const { guide } = await res.json();

    // if (!res.ok) {
    if (!guide || guide.title === undefined) {
        throw redirect(301, "/guides");
    }

    return {
        guide,
    };
}
